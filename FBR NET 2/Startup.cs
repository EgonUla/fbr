﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FBR_NET_2.Startup))]
namespace FBR_NET_2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
