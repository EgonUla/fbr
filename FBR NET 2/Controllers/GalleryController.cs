﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FBR_NET_2.Models;

namespace FBR_NET_2.Controllers
{

    [Authorize(Roles = "User, Admin")]
    
    
    public class GalleryController : MyController
    {
        // GET: Gallery

        protected FBREntities2 db = new FBREntities2();

        public ActionResult _List()
        {

            var list = db.Photos.OrderByDescending(x => x.Created).Take(10).ToList();

            return View();
        }
    }
}