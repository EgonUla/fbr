﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FBR_NET_2.Models;

namespace FBR_NET_2.Controllers
{

    [Authorize(Roles = "User, Admin")]
    public class PhotosController : MyController
    {
        
        //private FBREntities2 db = new FBREntities2();

        // GET: Photos
        public ActionResult Index()
        {
            CheckPerson();

            if (User.IsInRole("Admin"))
            {
                var photos = db.Photos.Include(p => p.Album);
                return View(photos.ToList());
            }

            else
            {
                

                var photos = CurrentPerson.Albums
                    .Select(x => x.Photos)
                    .SelectMany(x => x)
                    ;
                return View(photos.ToList());
            }
        }

        // GET: Photos/Details/5
        public ActionResult Details(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            return View(photo);
        }

        // GET: Photos/Create
        public ActionResult Create()
        {

            CheckPerson();

            


            ViewBag.AlbumId = new SelectList(CurrentPerson.Albums, "Id", "Name");

            return View(new Photo { PictureId = 0, Created = DateTime.Now });
        }

        // POST: Photos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Created,Name,AlbumId,Description,PictureId")] Photo photo, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                photo.Created = DateTime.Now;
                
                
                ChangeDataFile(file, x => photo.PictureId = x , null); // MINA LISASIn
                db.Photos.Add(photo);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.AlbumId = new SelectList(CurrentPerson.Albums, "Id", "Name", photo.AlbumId);
  
            return View(photo);
        }

        // GET: Photos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", photo.AlbumId);
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", photo.PictureId);
            return View(photo);
        }

        // POST: Photos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Created,Name,AlbumId,Description,PictureId")] Photo photo, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {

                db.Entry(photo).State = EntityState.Modified;

                ChangeDataFile(
                                file,
                                x => { photo.PictureId = x; db.SaveChanges(); },
                                photo.PictureId);

                db.SaveChanges();


                return RedirectToAction("Index");
            }
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", photo.AlbumId);
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", photo.PictureId);
            return View(photo);
        }

        // GET: Photos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            return View(photo);
        }

        // POST: Photos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Photo photo = db.Photos.Find(id);
            db.Photos.Remove(photo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
