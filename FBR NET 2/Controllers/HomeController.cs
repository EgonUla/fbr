﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FBR_NET_2.Models;


namespace FBR_NET_2.Controllers
{
    public class HomeController : MyController
    {
        
        
        
        public ActionResult Index()
            
            
        {
            CheckPerson();
            
            var photos = db.Photos.OrderByDescending(x => x.Created).Take(10);
            return View(photos.ToList());

           
        }


        [Authorize(Roles = "User, Admin")]
        public ActionResult Bron()
        {
            ViewBag.Message = "Broneeri";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}