﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FBR_NET_2.Models;

namespace FBR_NET_2.Controllers
{

    [Authorize(Roles = "User, Admin")]
    public class BookingsController : MyController
    {
        //private FBREntities2 db = new FBREntities2();

        // GET: Bookings

        
      

        public ActionResult Index()

           
        {
            CheckPerson();

            if (User.IsInRole("Admin"))
            {
                var bookings = db.Bookings.Include(b => b.Person).Include(b => b.Studio);
                return View(bookings.ToList());
            }
            else
            {
                var bookings = db.Bookings.Include(b => b.Person).Include(b => b.Studio)
                    .Where(x =>x.CreatorId == CurrentPerson.Id )
                    ;
                
                              
                return View(bookings.ToList());
            }
        }


       
        // GET: Bookings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }




        [Authorize(Roles = "User, Admin")]
        // GET: Bookings/Create
        public ActionResult Create(DateTime? dateTime, int? StudioId)
        {
            CheckPerson();

             dateTime = dateTime ?? DateTime.Today;

            

            DateTime fromDate = dateTime.Value;
            DateTime toDate = dateTime.Value.AddDays(1);

            var tunnid = db.Bookings.Where(x => x.BookingDate >= fromDate && x.BookingDate < toDate)
             .Select(x => new { x.BookingDate.Value.Hour, Hours = x.Hours??1 })
             .ToList()
             //;var tunnid = tunnid1
             .Select(x => Enumerable.Range(x.Hour, x.Hours))
             .SelectMany(x => x).ToArray();


            var Vabadajad = Enumerable.Range(10, 12)
                .Where(x => !tunnid.Contains(x))
                
                .ToArray();


            ViewBag.Vabadajad = new SelectList(Vabadajad);
            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email", CurrentPerson.Id);
            ViewBag.StudioId = new SelectList(db.Studios, "Id", "Name");



            return View(new Booking { BookingDate = dateTime.Value, CreatorId = CurrentPerson.Id });
        }


        [Authorize(Roles = "User, Admin")]
        // POST: Bookings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StudioId,CreatorId,BookingDate,Hours,Description")] Booking booking, int Vabadajad)
        {
            if (ModelState.IsValid)
            {
                booking.BookingDate = booking.BookingDate?.Date.AddHours(Vabadajad);
                db.Bookings.Add(booking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            // kuidas sa tagad, et TEISEL korral on Vabadajad ka VieBagis kaasas
            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email", booking.CreatorId);
            ViewBag.StudioId = new SelectList(db.Studios, "Id", "Name", booking.StudioId);
            return View(booking);
        }
        
        // GET: Bookings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email", booking.CreatorId);
            ViewBag.StudioId = new SelectList(db.Studios, "Id", "Name", booking.StudioId);
            return View(booking);
        }
       
        // POST: Bookings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StudioId,CreatorId,BookingDate,Hours,Description")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email", booking.CreatorId);
            ViewBag.StudioId = new SelectList(db.Studios, "Id", "Name", booking.StudioId);
            return View(booking);
        }
       
        // GET: Bookings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }
        
        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           
            Booking booking = db.Bookings.Find(id);

          
            db.Bookings.Remove(booking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
